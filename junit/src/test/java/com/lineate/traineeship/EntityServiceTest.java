package com.lineate.traineeship;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class EntityServiceTest {

    @ParameterizedTest
    @ValueSource(strings = {"32length_xxxxxxxxxxxxxxxxxxxxxxx", "x"})
    public void createEntityEntityNameIsCorrect(String s) {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, s, "Value");
        assertEquals(s, entityService.getEntity(s).getName());
    }

    @ParameterizedTest
    @ValueSource(strings = {"33length_xxxxxxxxxxxxxxxxxxxxxxxx", "32length xxxxxxxxxxxxxxxxxxxxxxx", ""})
    public void createEntityEntityNameIsNotCorrect(String s) {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, s, "Value");
        assertNull(entityService.getEntity(s));
    }

    @Test
    public void createEntity_mock() {
        EntityRepository repository = mock(EntityRepository.class);
        List<Entity> entities = new ArrayList<>();

        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService(repository);
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.write);
        Group group = userService.createGroup("x", permissions);
        User user = userService.createUser("x", group);


        doAnswer(invocation -> {
            Entity entity = (Entity) invocation.getArgument(0);
            entities.add(entity);
            return null;
        }).when(repository).save(any(Entity.class));
        entityService.createEntity(user, "entityname", "value");
        assertEquals(1, entities.size());

        when(repository.get(anyString())).thenReturn(entities.get(0));
        Entity actual = entityService.getEntity("xyz");
        assertEquals("entityname", actual.getName());
    }

    @Test
    public void name32Length_withoutSpaces() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "32length_xxxxxxxxxxxxxxxxxxxxxxx", "Value");
        Entity entity = entityService.getEntity("32length_xxxxxxxxxxxxxxxxxxxxxxx");
        assertEquals("32length_xxxxxxxxxxxxxxxxxxxxxxx", entity.getName());
    }

    @Test
    public void name1Length_withoutSpaces() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "x", "Value");
        Entity entity = entityService.getEntity("x");
        assertEquals("x", entity.getName());
    }

    @Test
    public void name33Length_withoutSpaces() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "33length_xxxxxxxxxxxxxxxxxxxxxxxx", "Value");
        assertNull(entityService.getEntity("33length_xxxxxxxxxxxxxxxxxxxxxxxx"));
    }

    @Test
    public void name32Length_withSpaces() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "32length xxxxxxxxxxxxxxxxxxxxxxx", "Value");
        assertNull(entityService.getEntity("32length xxxxxxxxxxxxxxxxxxxxxxx"));
    }

    @Test
    public void nameEmpty() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "", "Value");
        assertNull(entityService.getEntity(""));
    }

    @Test
    public void nameOnlySpaces() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, " ", "Value");
        assertNull(entityService.getEntity(" "));
    }

    @Test
    public void updateValue() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "x", "Value");
        Entity entity = entityService.getEntity("x");
        entity.setValue("Value_2");
        assertEquals("Value_2", entityService.getEntity("x").getValue());
    }

    @Test
    public void usernameNotNull() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("username", group);
        entityService.createEntity(user, "entityname", "value");
        assertNotNull(entityService.getEntity("entityname"));
    }

    @Test
    public void usernameIsNull() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser(null, group);
        entityService.createEntity(user, "entityname", "value");
        assertNull(entityService.getEntity("entityname"));
    }

    @Test
    public void groupnameNotNull() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("Group", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "entityname", "value");
        assertNotNull(entityService.getEntity("entityname"));
    }

    @Test
    public void groupnameIsNull() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup(null, permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "entityname", "value");
        assertNull(entityService.getEntity("entityname"));
    }

    @Test
    public void groupnameHasPermissions() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("x", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "entityname", "value");
        assertNotNull(entityService.getEntity("entityname"));
    }

    @Test
    public void groupnameHasNotPermissions() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        // List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("x", null);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "entityname", "value");
        assertNull(entityService.getEntity("entityname"));
    }

    @Test
    public void userIsAmemberOfAgroup() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("x", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "entityname", "value");
        assertNotNull(entityService.getEntity("entityname"));
    }

    @Test
    public void userIsNotAmemberOfAgroup() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        //List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        //Group group = userService.createGroup("x", permissions);
        User user = userService.createUser("x", null);
        entityService.createEntity(user, "entityname", "value");
        assertNull(entityService.getEntity("entityname"));
    }

    @Test
    public void userIsCreatorAllPermissionsCanRead() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("x", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "entityname", "value");
        Entity entity = entityService.getEntity("entityname");
        assertEquals("value", entityService.getEntityValue(user, "entityname"));
    }

    @Test
    public void userIsCreatorReadPermissionsCanRead() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read);
        Group group = userService.createGroup("x", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "entityname", "value");
        Entity entity = entityService.getEntity("entityname");
        assertEquals("value", entityService.getEntityValue(user, "entityname"));
    }

    @Test
    public void userIsCreatorWritePermissionsCanRead() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.write);
        Group group = userService.createGroup("x", permissions);
        User user = userService.createUser("x", group);
        entityService.createEntity(user, "entityname", "value");
        Entity entity = entityService.getEntity("entityname");
        assertEquals("value", entityService.getEntityValue(user, "entityname"));
    }

    @Test
    public void userIsNotCreatorAllPermissionsCanRead() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group group = userService.createGroup("x", permissions);
        User user = userService.createUser("x", group);
        User userOther = userService.createUser("y", group);
        entityService.createEntity(user, "entityname", "value");
        Entity entity = entityService.getEntity("entityname");
        assertEquals("value", entityService.getEntityValue(userOther, "entityname"));
    }

    @Test
    public void userIsNotCreatorReadPermissionsCanRead() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read);
        Group group = userService.createGroup("x", permissions);
        User user = userService.createUser("x", group);
        User userOther = userService.createUser("y", group);
        entityService.createEntity(user, "entityname", "value");
        Entity entity = entityService.getEntity("entityname");
        assertEquals("value", entityService.getEntityValue(userOther, "entityname"));
    }

    @Test
    public void userIsNotCreatorWritePermissionsCanNotRead() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.write);
        Group groupInner = userService.createGroup("x", permissions);
        User userCreator = userService.createUser("x", groupInner);
        User userOther = userService.createUser("y", groupInner);
        entityService.createEntity(userCreator, "entityname", "value");
        Entity entity = entityService.getEntity("entityname");
        assertNull(entityService.getEntityValue(userOther, "entityname"));
    }

    @Test
    public void userIsNotCreatorAllPermissionsCanWrite() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read, Permission.write);
        Group groupInner = userService.createGroup("x", permissions);
        User userCreator = userService.createUser("x", groupInner);
        User userOther = userService.createUser("y", groupInner);
        entityService.createEntity(userCreator, "entityname", "value");
        Entity entity = entityService.getEntity("entityname");
        entityService.updateEntity(userOther, "entityname", "value_2");
        assertEquals("value_2", entityService.getEntityValue(userOther, "entityname"));
    }

    @Test
    public void userIsNotCreatorReadPermissionsCanNotWrite() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.read);
        Group groupInner = userService.createGroup("x", permissions);
        User userCreator = userService.createUser("x", groupInner);
        User userOther = userService.createUser("y", groupInner);
        entityService.createEntity(userCreator, "entityname", "value");
        Entity entity = entityService.getEntity("entityname");
        entityService.updateEntity(userOther, "entityname", "value_2");
        assertEquals("value", entityService.getEntityValue(userOther, "entityname"));
    }

    @Test
    public void userIsNotCreatorWritePermissionsCanWrite() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.write);
        Group groupInner = userService.createGroup("x", permissions);
        User userCreator = userService.createUser("x", groupInner);
        User userOther = userService.createUser("y", groupInner);
        entityService.createEntity(userCreator, "entityname", "value");
        Entity entity = entityService.getEntity("entityname");
        entityService.updateEntity(userOther, "entityname", "value_2");
        assertEquals("value_2", entityService.getEntityValue(userCreator, "entityname"));
    }

    @Test
    public void entityHasGroupsInWhichUserIsMember() {
        ServiceFactory serviceFactory = new ServiceFactory();
        EntityService entityService = serviceFactory.createEntityService();
        UserService userService = new ServiceFactory().createUserService();
        List<Permission> permissions = Arrays.asList(Permission.write);
        Group groupInner = userService.createGroup("x", permissions);
        Group groupInner2 = userService.createGroup("y", permissions);
        Group groupInner3 = userService.createGroup("z", permissions);
        User userCreator = userService.createUser("x", groupInner);
        groupInner2.addUser(userCreator);
        groupInner3.addUser(userCreator);
        entityService.createEntity(userCreator, "entityname", "value");
        Entity entity = entityService.getEntity("entityname");
        assertEquals(3, userCreator.getGroups().size());
        assertEquals(3, entityService.getEntity("entityname").getGroups().size());
    }
}
